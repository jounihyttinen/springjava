/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hello.models;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Hyttjo
 */

public interface MemberDAO extends CrudRepository<Member, Integer> {

    public Member findByFirstName(String firstName);
    
    public Member findByLastName(String lastName);
}
